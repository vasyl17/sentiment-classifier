# Sentiment classifier

Sentiment classifier that determines if a user review is positive (4-5 stars) or negative (1-3 stars)