import requests, re, random, json, spacy
from lxml import html
from nltk.corpus import sentiwordnet as swn
nlp = spacy.load("en_core_web_md", disable=["textcat"])


def accuracy(test_data):
    tp = 0   
    print('Analyzing reviews')
    for review, sentiment in test_data:
        print('.', end='')
        output = analyze_sentiment(review)
        if output == sentiment:
            tp += 1
    print()
    return tp / len(test_data)


#def analyze_sentiment_baseline(review):
#    return random.choice(["positive", "negative"])


def is_notional(token):
    return re.findall(r'^(?:NN|VB|JJ|RB)', token.tag_)


def pos_to_wn(pos):
    if pos.startswith('NN'):
        return 'n'
    if pos.startswith('VB'):
        return 'v'
    if pos.startswith('JJ'):
        return 'a'
    if pos.startswith('RB'):
        return 'r'

def analyze_sentiment(review):
    sentiment = 0
    
    for token in nlp(review):
        if is_notional(token):
            #print(token.text)
            word = token.lemma_.lower()
            word_pos_wn = pos_to_wn(token.tag_)
            try:
                sentiment += list(swn.senti_synsets(word, word_pos_wn))[0].pos_score()
                sentiment -= list(swn.senti_synsets(word, word_pos_wn))[0].neg_score()
                #print(list(swn.senti_synsets(word, word_pos_wn))[0])
            except:
                pass
    
    #print(sentiment)
    if sentiment > 0:
        return 'positive'
    else:
        return 'negative'


def parse_sent(sent):
    sentence = nlp(sent)
    for token in sentence:
        print(token.lemma_.lower(), token.tag_, is_notional(token))


reviews = []
for page in range(1, 21):
    print('{}/20 pages scraped'.format(page))
    url = f'https://www.consumeraffairs.com/health_clubs/planet_fitness.html?page={page}'
    web_page = requests.get(url)
    tree = html.fromstring(web_page.text)
    data = tree.xpath('//div[@class="rvw__hdr-stat"]//img/@data-rating|//div[@class="rvw-bd"]//p/text()')
    reviews.extend(data)

reviews_string = '\n'.join(reviews)
reviews = re.split(r'\n(?=\d\.0\n)', reviews_string)

review_sentiment_pairs = []
for r in reviews:
    text = r[4:]
    sentiment = r[0]
    if int(sentiment) > 3:
        sentiment = 'positive'
    else:
        sentiment = 'negative'
    review_sentiment_pairs.append((text, sentiment))

random.shuffle(review_sentiment_pairs)    
reviews_positive = []
reviews_negative = []

for review, sentiment in review_sentiment_pairs:
    if sentiment == 'positive':
        reviews_positive.append((review, sentiment))
    else:
        reviews_negative.append((review, sentiment))
    
test_set = reviews_positive[:50] + reviews_negative[:50]
dev_set = reviews_positive[50:] + reviews_negative[50:]

with open("test-set.json", "w", encoding="utf-8") as f:
    json.dump(test_set, f)
with open("dev-set.json", "w", encoding="utf-8") as f:
    json.dump(dev_set, f)

print('Accuracy: {}'.format(accuracy(dev_set)))